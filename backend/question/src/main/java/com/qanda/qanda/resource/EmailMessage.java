package com.qanda.qanda.resource;


public class EmailMessage {
	public String To;
	public String Subject;
	public String Message;
	
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getMessage() {
		return Message;
	}
	
	public void setMessage(String message) {
		Message = message;
	}
	public EmailMessage(String to, String subject, String message) {

		To = to;
		Subject = subject;
		Message = message;
	}
	public EmailMessage() {
		
	}
	

}
